package hk.quantr.opener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;

@ActionID(
		category = "Edit",
		id = "hk.quantr.opener.OpenTerminalAction"
)
@ActionRegistration(
		iconBase = "hk/quantr/opener/application_xp_terminal.png",
		displayName = "#CTL_OpenTerminalAction"
)
@ActionReferences(
		{
			@ActionReference(path = "Menu/Opener", position = 0, separatorBefore = -50, separatorAfter = 50),
			@ActionReference(path = "Editors/Popup", position = 0, separatorBefore = -50, separatorAfter = 50),
			@ActionReference(path = "Projects/Actions", position = 0, separatorBefore = -50, separatorAfter = 50),
			@ActionReference(path = "Projects/package/Actions", position = 0, separatorBefore = -50, separatorAfter = 50),
			@ActionReference(path = "Loaders/text/x-java/Actions", position = 0, separatorBefore = -50, separatorAfter = 50)
		})
@Messages("CTL_OpenTerminalAction=Open Terminal")
public final class OpenTerminalAction implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(null, "hi");
	}
}
